const Account = require('sc_sdk/lib/account')

const data = {
  "config": {
    backend: 'https://api-qc.lina.supply',
    blockchain: 'https://node1-qc.lina.supply',
    languageId: 1, //vi-vn
    intervalCheck: 500,
    checkTimes: 10,
    isRoot: false,
    "env": "dev"
  }
}

const main = async () => {
  const result = {
    email: "LILI.tran@lina.network",
    password: "123abc!!",
  };

  const account = new Account(data.config);
  await account.login(result.email, result.password);
  console.log(`====login account=====${result.email}`)
  console.log(`address=${account.Address}  privateKey=${account.PrivateKey}}`);

  console.log('=======chọn nông trại======')
  const { data: farms } = await account.getWorkingFarms();
  console.log(`list working farm is ${JSON.stringify(farms)}`);

  if (farms.length === 0) return;
  const farm = farms[0]
  account.ID = farm.id; // chọn farm 1

  // const temp = await account.loadAllMethods();
  // console.log(`=====methods: ${JSON.stringify(temp)}`);
  await manageFarms(account);
};

const manageFarms = async (account) => {
  console.log('=======thông tin nông trại======')
  const farm = await account.queryData('getFarm')
  console.log(`${JSON.stringify(farm)}`)
  const { data: sections } = await account.queryData('getSections', {
    from: 0,
    limit: 10
  })
  console.log('=======end thông tin nông trại======')
  if (sections.length != 0){
    console.log('======chi tiet vuon====')
    const section = await account.queryData('getSection', {
      customs: [{ 
        "@type": "proto.Int32Message", 
        "message": sections[0].id
      }]
    })
    const { data: arableLands } = await account.queryData('getArableLandsBySectionId', {
      customs: [{ 
        "@type": "proto.Int32Message", 
        "message": section.id
      }]
    })
    console.log('======end chi tiet vuon====')
  }
  console.log('=======kho lưu trữ======')
  const { data: archives} = await account.queryData('getArchives', {
    from: 0,
    limit: 10
  })
  console.log('=======end kho lưu trữ======')

  console.log('=======thành phẩm======')
  const { data: finishedProductFarms } = await account.queryData('getFinishedProductFarms', {
    from: 0,
    limit: 10
  })
  if (finishedProductFarms.length !== 0) {
    const { data: gradeds } = await account.queryData('getGradedListByFinishedProductFarmId', {
      from: 0,
      limit: 10,
      customs: [{ 
        "@type": "proto.Int32Message", 
        "message": finishedProductFarms[0].id
      }]
    })
  }
  console.log('=======end thành phẩm======')

  console.log('=======chứng chỉ======')
  const { data: certificates } = await account.queryData('getFarmCertificates', { 
    from: 0,
    limit: 10,
  })
  console.log('=======end chứng chỉ======')

  console.log('=======kết quả thí nghiệm======')
  const { data: soilTestResults } = await account.queryData('getSoilTestResults', { 
    from: 0,
    limit: 10,
  })
  const { data: waterTestResults } = await account.queryData('getWaterTestResults', { 
    from: 0,
    limit: 10,
  })
  const { data: finishedProductTestResults } = await account.queryData('getFinishedProductTestResults', { 
    from: 0,
    limit: 10,
  })
  console.log('=======end kết quả thí nghiệm======')  
}

main()
  .then(function () {
    console.log(
      "=============================================================================================================================="
    );
  })
  .catch(function (err) {
    console.error(err);
    console.log(
      "=============================================================================================================================="
    );
  });